package com.sixsimplex.mcgm;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

@Path("/services")
public class ServicesResource
{
	@Context
	private UriInfo uriInfo;
	
	public ServicesResource(@Context ServletContext servletContext)
	{
		
	}
	
	@Path("/service1")
	@GET
	@Produces("text/plain")
	public String service1()
	{
		return "service1";
	}
	
	@Path("/service2")
	@GET
	@Produces("text/plain")
	public String service2()
	{
		return "service2";
	}
	
	@Path("/service3")
	@GET
	@Produces("text/plain")
	public String service3()
	{
		return "service3";
	}
}